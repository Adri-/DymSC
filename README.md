# DymSC

![do you mean scroll speed ?](https://puu.sh/E50bV/9aa0b8d71b.png "ppy") (image gone because puush)

# Requirements 

- Java
- Brain
- Osu beatmaps

How to use

1 - Launch the program by clicking the "DymSC" shortcut. If you want logs, you can run 'java -jar DymSC.jar' from the /bin folder

2 - Use brain

# Downloads

Latest version permanent link : https://osudaily.net/dl/DymSC.rar