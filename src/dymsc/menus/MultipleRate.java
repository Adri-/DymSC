/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dymsc.menus;

import dymsc.customClasses.Beatmap;
import dymsc.customClasses.ConvertMP3;
import dymsc.customClasses.DisabledItemSelectionModel;
import dymsc.customClasses.JavaFXFileDialog;
import dymsc.customClasses.RatingMonitor;
import com.sun.jna.platform.FileUtils;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static java.nio.file.StandardCopyOption.*;

/**
 *
 * @author adrien
 */
public class MultipleRate extends javax.swing.JFrame {

    // 0 = processing
    // 1 = back
    // 2 = forward
    int state;

    ArrayList<Beatmap> currentBeatmaps;
    String directory;
    String gamedirectory;
    String packdirectory;
    ArrayList<Float> rates;
    
    ArrayList<Float> progress;

    String diffedit;
    Beatmap editingBeatmap;
    private Beatmap model;

    String osu_folder;
    /**
     * Creates new form Rate
     *
     * @param currBeatmaps
     * @param directory
     */
    public MultipleRate(ArrayList<Beatmap> currBeatmaps, String directory, String osu_folder) {
        this.currentBeatmaps = currBeatmaps;
        this.directory = directory;
        this.packdirectory = directory;
        this.osu_folder = osu_folder;
        initComponents();
        init();
    }

    public void init() {
        jListPendingRates.setSelectionModel(new DisabledItemSelectionModel());
        jButtonSelect.setEnabled(false);

        this.rates = new ArrayList<Float>();
        this.state = 0;

        if (this.currentBeatmaps.size() == 1) {
            jLabelDifficulty.setText("Rating");
        }

        jButtonStartRating.addActionListener((ActionEvent ae) -> {
            Runnable rm = new RatingMonitor(this);
            new Thread(rm).start();
        });

        jCheckBoxIncludeAudio.addActionListener((ActionEvent az) -> {
            jInputCustomOffset.setEnabled(!jCheckBoxIncludeAudio.isSelected());
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelBG = new javax.swing.JPanel();
        jInputUnit = new javax.swing.JTextField();
        jInputAudioFilename = new javax.swing.JTextField();
        jAddRate = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListPendingRates = new javax.swing.JList();
        jLabel3 = new javax.swing.JLabel();
        jButtonStartRating = new javax.swing.JButton();
        jLabelDifficulty = new javax.swing.JLabel();
        jButtonBack = new javax.swing.JButton();
        jCheckBoxIncludeAudio = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jInputCustomOffset = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jCheckBoxNightcore = new javax.swing.JCheckBox();
        jButtonSelect = new javax.swing.JButton();
        jProgressBarRate = new javax.swing.JProgressBar();
        jLabel6 = new javax.swing.JLabel();
        jInputCustomBitrate = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelBG.setBackground(new Color(0.1f, 0.1f, 0.1f, 0f));
        jPanelBG.setPreferredSize(new java.awt.Dimension(300, 350));

        jInputUnit.setText("1.0");
        jInputUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jInputUnitActionPerformed(evt);
            }
        });

        jInputAudioFilename.setText("audio");

        jAddRate.setText("Add");
        jAddRate.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jAddRate.setMaximumSize(new java.awt.Dimension(20, 20));
        jAddRate.setMinimumSize(new java.awt.Dimension(20, 20));
        jAddRate.setPreferredSize(new java.awt.Dimension(20, 20));
        jAddRate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jAddRateMouseClicked(evt);
            }
        });
        jAddRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jAddRateActionPerformed(evt);
            }
        });

        jLabel1.setText("Audio file prefix");

        jLabel2.setText("Add a rate");

        jScrollPane1.setViewportView(jListPendingRates);

        jLabel3.setText("Pending rates :");

        jButtonStartRating.setText("Validate");
        jButtonStartRating.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonStartRatingMouseClicked(evt);
            }
        });
        jButtonStartRating.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartRatingActionPerformed(evt);
            }
        });

        jLabelDifficulty.setText("Multiple Editing");

        jButtonBack.setText("Back");
        jButtonBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonBackMouseClicked(evt);
            }
        });
        jButtonBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBackActionPerformed(evt);
            }
        });

        jCheckBoxIncludeAudio.setSelected(true);
        jCheckBoxIncludeAudio.setText("Include Audio");

        jLabel4.setText("Custom offset");

        jInputCustomOffset.setText("0");

        jLabel5.setText("ms");

        jCheckBoxNightcore.setText("Nightcore");
        jCheckBoxNightcore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxNightcoreActionPerformed(evt);
            }
        });

        jButtonSelect.setText("Select Beatmap");
        jButtonSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelectActionPerformed(evt);
            }
        });

        jProgressBarRate.setString("");
        jProgressBarRate.setStringPainted(true);

        jLabel6.setText("Custom bitrate");

        jInputCustomBitrate.setText("128");
        jInputCustomBitrate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jInputCustomBitrateActionPerformed(evt);
            }
        });

        jLabel7.setText("kbps");

        javax.swing.GroupLayout jPanelBGLayout = new javax.swing.GroupLayout(jPanelBG);
        jPanelBG.setLayout(jPanelBGLayout);
        jPanelBGLayout.setHorizontalGroup(
            jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBGLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jInputAudioFilename, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelBGLayout.createSequentialGroup()
                        .addComponent(jInputCustomOffset, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5))
                    .addGroup(jPanelBGLayout.createSequentialGroup()
                        .addComponent(jInputUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jAddRate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBGLayout.createSequentialGroup()
                        .addComponent(jInputCustomBitrate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)))
                .addGap(26, 26, 26))
            .addGroup(jPanelBGLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonBack)
                .addGap(18, 18, 18)
                .addComponent(jLabelDifficulty)
                .addGap(26, 26, 26)
                .addComponent(jButtonSelect, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanelBGLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBGLayout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(jButtonStartRating)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE))
                    .addGroup(jPanelBGLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxIncludeAudio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBoxNightcore)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                    .addComponent(jProgressBarRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelBGLayout.setVerticalGroup(
            jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBGLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelDifficulty)
                    .addComponent(jButtonBack)
                    .addComponent(jButtonSelect))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jInputAudioFilename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jInputCustomOffset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jInputCustomBitrate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(7, 7, 7)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jInputUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jAddRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelBGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jCheckBoxIncludeAudio)
                    .addComponent(jCheckBoxNightcore))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBarRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonStartRating)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelBG, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelBG, javax.swing.GroupLayout.PREFERRED_SIZE, 422, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStartRatingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStartRatingActionPerformed

    }//GEN-LAST:event_jButtonStartRatingActionPerformed

    private void jAddRateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jAddRateMouseClicked

    }//GEN-LAST:event_jAddRateMouseClicked

    private void jInputUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jInputUnitActionPerformed

    }//GEN-LAST:event_jInputUnitActionPerformed

    private void jButtonBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBackActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonBackActionPerformed

    private void jButtonBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonBackMouseClicked
        this.state = 1;
    }//GEN-LAST:event_jButtonBackMouseClicked

    private void jButtonStartRatingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonStartRatingMouseClicked

    }//GEN-LAST:event_jButtonStartRatingMouseClicked

    private void jAddRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jAddRateActionPerformed
        float rate = 1;
        try {
            rate = Float.valueOf(this.jInputUnit.getText());
        } catch (Exception e) {

        }
        if (rate > 0 && rate <= 100 && rate != 1) {
            try {
                this.jButtonSelect.setEnabled(false);
                if (this.rates == null) {
                    this.rates = new ArrayList<Float>();
                }
                if (!this.rates.contains(rate)) {
                    this.rates.add(rate);
                }
            } catch (Exception ex) {
                Logger.getLogger(MultipleRate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        updateRateList();
    }//GEN-LAST:event_jAddRateActionPerformed

    private void jCheckBoxNightcoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxNightcoreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxNightcoreActionPerformed

    private void jButtonSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSelectActionPerformed
        toggleWorking(true);
        File selection = JavaFXFileDialog.chooseFileWithJavaFXDialog(this.osu_folder);
        boolean valid = false;
        if (selection != null && selection.isFile()) {
            if (selection.getName().length() > 3) {
                if ("osu".equals(selection.getName().substring(selection.getName().length() - 3))) {
                    valid = true;
                }
            }
        }
        if (!valid) {
            JOptionPane.showMessageDialog(null, "Invalid file.");
        } else {
            try {
                this.editingBeatmap = new Beatmap(selection);
                this.directory = selection.getParent() + "\\";
                this.gamedirectory = new File(this.directory).getParent();
                this.jInputAudioFilename.setText(this.editingBeatmap.getTitle());
                this.diffedit = this.editingBeatmap.getVersion();
                this.editingBeatmap.takeModel(this.model);
                this.packdirectory = this.gamedirectory + "\\" + this.model.getTitle() + "\\";
                File prepareDir = new File(this.packdirectory);
                if (!prepareDir.exists()) {
                    prepareDir.mkdir();
                }
                if (editingBeatmap.getBackgroundName() != null) {
                    try {
                        Files.copy(Paths.get(this.directory + editingBeatmap.getBackgroundName()), Paths.get(this.packdirectory + editingBeatmap.getBackgroundName()));
                    } catch (Exception e) {

                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MultipleRate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        toggleWorking(false);
    }//GEN-LAST:event_jButtonSelectActionPerformed

    private void jInputCustomBitrateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jInputCustomBitrateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jInputCustomBitrateActionPerformed

    public void toggleWorking(boolean work) {
        jButtonStartRating.setEnabled(!work);
        jInputAudioFilename.setEnabled(!work);
        jInputUnit.setEnabled(!work);
        jInputCustomOffset.setEnabled(!work);
        jAddRate.setEnabled(!work);
        jCheckBoxIncludeAudio.setEnabled(!work);
        jListPendingRates.setEnabled(!work);
        jCheckBoxNightcore.setEnabled(!work);
        jInputCustomBitrate.setEnabled(!work);
        this.revalidate();
        this.repaint();
    }

    @Override
    public int getState() {
        return this.state;
    }

    public void cleanBeforeForce() {
        this.jCheckBoxIncludeAudio.setSelected(false);
        new File(this.directory + "TEMP.wav").delete();
    }

    public void startRating() {
        this.toggleWorking(true);
        try {
            if (this.editingBeatmap != null) {
                passRates(new ArrayList<Beatmap>(Arrays.asList(editingBeatmap)));
                this.jButtonSelect.setEnabled(true);
            } else {
                passRates(this.currentBeatmaps);
            }
        } catch (Exception ex) {
            Logger.getLogger(MultipleRate.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.toggleWorking(false);
    }

    public void updateRateList() {
        DefaultListModel listModel = new DefaultListModel();
        int tracker = 0;
        for (Float rate : this.rates) {
            String name = "All";
            if (this.currentBeatmaps.size() == 1) {
                name = this.currentBeatmaps.get(0).getVersion();
            }
            if (this.diffedit != null) {
                name = this.diffedit;
            }
            listModel.add(tracker, name + " x" + Float.toString(rate));
            tracker++;
        }
        jListPendingRates.setModel(listModel);
        this.revalidate();
        this.repaint();
    }

    public void passRates(ArrayList<Beatmap> beatmaps) throws Exception {
        this.progress = new ArrayList<Float>();
        
        int index = 0;
        for (Float rate : this.rates) {
            int decal = Integer.valueOf(this.jInputCustomOffset.getText());
            String nc = "";
            if(jCheckBoxNightcore.isSelected()) {
                nc = " NC";
            }
            String audio = this.jInputAudioFilename.getText() + " x" + Float.toString(rate) + nc + ".mp3";
            
                if (this.jCheckBoxIncludeAudio.isSelected()) {
                    this.progress.add(new Float(0));
                    ConvertMP3.createMp3(this.directory + beatmaps.get(0).getAudioFileName(), this.packdirectory + audio, new Float(rate), jCheckBoxNightcore.isSelected(), index, this, Integer.valueOf(jInputCustomBitrate.getText()));
                    index++;
                }
            for (Beatmap beatmap : beatmaps) {
                createRate(beatmap, audio, rate, decal, jCheckBoxNightcore.isSelected());
            }
        }
        this.rates.clear();
        updateRateList();
    }

    public void createRate(Beatmap currentBeatmap, String audio, float rate, int decal, boolean nightcore) throws Exception {
        Beatmap newMap = currentBeatmap.getRatedBeatmap(rate, decal, nightcore);
        newMap.setAudioFileName(audio);
        newMap.writeToFile(new File(this.packdirectory + newMap.getFilename().replaceAll("[\\\\*/\\\\\\\\!\\\\|:?<>]", "")));
    }

    /**
     * @param currBeatmaps
     * @param directory
     */
    public static void main(ArrayList<Beatmap> currBeatmaps, String directory, String osu_folder) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MultipleRate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MultipleRate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MultipleRate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MultipleRate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MultipleRate(currBeatmaps, directory, osu_folder).setVisible(true);
        });
    }

    public JButton getjAddRate() {
        return jAddRate;
    }

    public void setjAddRate(JButton jAddRate) {
        this.jAddRate = jAddRate;
    }

    public JButton getjButtonBack() {
        return jButtonBack;
    }

    public void setjButtonBack(JButton jButtonBack) {
        this.jButtonBack = jButtonBack;
    }

    public ArrayList<Float> getProgresses() {
        return this.progress;
    }
    
    public JButton getjButtonSelect() {
        return jButtonSelect;
    }

    public void setjButtonSelect(JButton jButtonSelect) {
        this.jButtonSelect = jButtonSelect;
    }

    public JButton getjButtonStartRating() {
        return jButtonStartRating;
    }

    public void setjButtonStartRating(JButton jButtonStartRating) {
        this.jButtonStartRating = jButtonStartRating;
    }

    public JCheckBox getjCheckBoxIncludeAudio() {
        return jCheckBoxIncludeAudio;
    }

    public void setjCheckBoxIncludeAudio(JCheckBox jCheckBoxIncludeAudio) {
        this.jCheckBoxIncludeAudio = jCheckBoxIncludeAudio;
    }

    public JCheckBox getjCheckBoxNightcore() {
        return jCheckBoxNightcore;
    }

    public void setjCheckBoxNightcore(JCheckBox jCheckBoxNightcore) {
        this.jCheckBoxNightcore = jCheckBoxNightcore;
    }

    public JTextField getjInputAudioFilename() {
        return jInputAudioFilename;
    }

    public void setjInputAudioFilename(JTextField jInputAudioFilename) {
        this.jInputAudioFilename = jInputAudioFilename;
    }

    public JTextField getjInputCustomOffset() {
        return jInputCustomOffset;
    }

    public void setjInputCustomOffset(JTextField jInputCustomOffset) {
        this.jInputCustomOffset = jInputCustomOffset;
    }

    public JTextField getjInputUnit() {
        return jInputUnit;
    }

    public void setjInputUnit(JTextField jInputUnit) {
        this.jInputUnit = jInputUnit;
    }

    public JLabel getjLabel1() {
        return jLabel1;
    }

    public void setjLabel1(JLabel jLabel1) {
        this.jLabel1 = jLabel1;
    }

    public JLabel getjLabel2() {
        return jLabel2;
    }

    public void setjLabel2(JLabel jLabel2) {
        this.jLabel2 = jLabel2;
    }

    public JLabel getjLabel3() {
        return jLabel3;
    }

    public void setjLabel3(JLabel jLabel3) {
        this.jLabel3 = jLabel3;
    }

    public JLabel getjLabel4() {
        return jLabel4;
    }

    public void setjLabel4(JLabel jLabel4) {
        this.jLabel4 = jLabel4;
    }

    public JLabel getjLabel5() {
        return jLabel5;
    }

    public void setjLabel5(JLabel jLabel5) {
        this.jLabel5 = jLabel5;
    }

    public JLabel getjLabelDifficulty() {
        return jLabelDifficulty;
    }

    public void setjLabelDifficulty(JLabel jLabelDifficulty) {
        this.jLabelDifficulty = jLabelDifficulty;
    }

    public JList getjListPendingRates() {
        return jListPendingRates;
    }

    public void setjListPendingRates(JList jListPendingRates) {
        this.jListPendingRates = jListPendingRates;
    }

    public Beatmap getEditingBeatmap() {
        return this.editingBeatmap;
    }

    public void setEditingBeatmap(Beatmap beatmap) {
        this.editingBeatmap = beatmap;
    }

    public Beatmap getModel() {
        return model;
    }

    public void setModel(Beatmap model) {
        this.model = model;
    }
    
    public void setProgress() {
        float total = 0;
        int nb = this.progress.size();
        int done = 0;
        
        for(Float p: this.progress) {
            total += p;
            if(p*100>=99) {
                done++;
            }
        }
        int percent = Math.round(total / nb * 100);
        this.jProgressBarRate.setValue(percent);
        this.jProgressBarRate.setString(String.valueOf(done)+"/"+String.valueOf(nb));    
        
        if(done == nb) {
            this.jProgressBarRate.setValue(0);
            this.jProgressBarRate.setString("");    
            this.toggleWorking(false);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jAddRate;
    private javax.swing.JButton jButtonBack;
    private javax.swing.JButton jButtonSelect;
    private javax.swing.JButton jButtonStartRating;
    private javax.swing.JCheckBox jCheckBoxIncludeAudio;
    private javax.swing.JCheckBox jCheckBoxNightcore;
    private javax.swing.JTextField jInputAudioFilename;
    private javax.swing.JTextField jInputCustomBitrate;
    private javax.swing.JTextField jInputCustomOffset;
    private javax.swing.JTextField jInputUnit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabelDifficulty;
    private javax.swing.JList jListPendingRates;
    private javax.swing.JPanel jPanelBG;
    private javax.swing.JProgressBar jProgressBarRate;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
