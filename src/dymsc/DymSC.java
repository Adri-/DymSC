package dymsc;

import dymsc.customClasses.Beatmap;
import dymsc.customClasses.DymSCProperties;
import dymsc.customClasses.getPropertyValues;
import dymsc.menus.BeatmapDiffs;
import dymsc.menus.BeatmapSelectionAndCreate;
import dymsc.menus.MultipleRate;
import dymsc.menus.MultipleRatePack;
import dymsc.menus.NewSet;
import dymsc.menus.Welcome;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author adrien
 */
public class DymSC {

    private static int fluid = 20;
    private static String title;
    private static String iconPath;
    private static Image ic;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        DymSC.title = "DymSC";
        DymSC.iconPath = "/assets/icon.png";
        DymSC.ic = new ImageIcon(DymSC.class.getResource(DymSC.iconPath)).getImage();
        String version = "b0.8";
        
        boolean isRunning = true;

        Welcome welcome = new Welcome();
        welcome.show();
        
        if(!Utils.checkFFMPEG()) {
            welcome.showMessage("ffmpeg not found, audio convertion will be disabled");
        }
        
        DymSCProperties props = new DymSCProperties();
        try {
            props = new getPropertyValues().getPropValues();
            if("".equals(props.osu_folder)) {
                welcome.showMessage("osu! folder not set : You can change it in bin/config.properties !");
            }
        } catch(IOException e) {
            welcome.showMessage("error loading config file: "+e);
        }
        
        
        sleep(1000);

        welcome.setVisible(false);
        welcome.dispose();

        while (isRunning) {

            BeatmapSelectionAndCreate bmS = new BeatmapSelectionAndCreate(version, props.osu_folder);
            bmS.setTitle(title);
            bmS.setSize(300, 140);
            bmS.setIconImage(DymSC.ic);
            bmS.show();

            while (!bmS.isDone()) {
                sleep(DymSC.fluid);
            }

            switch (bmS.getState()) {
                case 2:
                    // Chose a single beatmap
                    singleBeatmap(bmS, props);
                    break;
                case 3:
                    // Chose to create a new set
                    bmS.setVisible(false);
                    bmS.dispose();
                    newSet(props);
                    break;

                default:
            }
        }
    }

    private static void singleBeatmap(BeatmapSelectionAndCreate bmS, DymSCProperties props) throws IOException, InterruptedException {

        String path = bmS.getSelectedPath();

        ArrayList<Beatmap> selectedBeatmaps = new ArrayList<Beatmap>();
        File[] files = new File(path).listFiles();

        for (File file : files) {
            if ("osu".equals(Utils.getFileExtension(file))) {
                selectedBeatmaps.add(new Beatmap(file.getName(), (ArrayList) Files.readAllLines(file.toPath(), StandardCharsets.UTF_8)));
            }
        }

        String currentDir = bmS.getActiveDirectory();

        bmS.setVisible(false);
        bmS.dispose();

        boolean selectingDiff = true;

        while (selectingDiff) {
            BeatmapDiffs selectDiffs = new BeatmapDiffs(selectedBeatmaps, currentDir);
            selectDiffs.setTitle(DymSC.title + " - Diff Select");
            selectDiffs.setIconImage(DymSC.ic);
            selectDiffs.show();

            while (selectDiffs.getState() == 0) {
                sleep(fluid);
            }

            if (selectDiffs.getState() == 2) {
                selectDiffs.setVisible(false);
                ArrayList<Beatmap> selectedDifficulties = selectDiffs.getSelectedBeatmaps();

                if (selectedDifficulties.size() >= 1) {
                    MultipleRate rate = new MultipleRate(selectedDifficulties, currentDir, props.osu_folder);
                    rate.setTitle(DymSC.title + " - Rating");
                    rate.setIconImage(DymSC.ic);
                    rate.show();

                    while (rate.getState() == 0) {
                        sleep(fluid);
                    }

                    if (rate.getState() == 1) {
                        selectDiffs.setState(0);
                        rate.setVisible(false);
                    }
                }

            } else {
                selectingDiff = false;
                selectDiffs.dispose();
            }
        }
    }

    private static void newSet(DymSCProperties props) throws InterruptedException, IOException {
        NewSet createSet = new NewSet();
        createSet.setTitle(DymSC.title + " - New Set");
        createSet.setIconImage(DymSC.ic);
        while (createSet.getState() != 1) {
            createSet.show();

            while (createSet.getState() == 0) {
                sleep(fluid);
            }

            if (createSet.getState() == 2) {
                Beatmap newSet = createSet.getNewSet();

                MultipleRatePack packing = new MultipleRatePack(newSet, props.osu_folder);

                createSet.setVisible(false);
                packing.setIconImage(DymSC.ic);
                packing.setTitle(DymSC.title + " - Add maps");
                packing.show();

                while (packing.getState() == 0) {
                    sleep(fluid);
                }

                packing.setVisible(false);
                packing.dispose();
                createSet.setState(0);
            }
        }
        createSet.dispose();
    }

}
