package dymsc.customClasses;

import dymsc.menus.MultipleRate;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import javax.swing.JProgressBar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ADRIEN
 */
abstract public class ConvertMP3 {

    public static void createMp3(String input, String output, float rate, boolean nightcore, int index, MultipleRate window, int bitrate) throws IOException {

        ProcessBuilder pb;
        if (!nightcore) {

            String atempo = "";
            ArrayList<Float> mults = new ArrayList<>();

            if (rate > 2) {
                float lastmult = rate;

                do {
                    lastmult /= 2;
                    mults.add((float) 2.0);
                } while (lastmult > 2);

                mults.add(lastmult);
            } else if (rate < 0.5) {
                float lastmult = rate;

                do {
                    lastmult *= 2;
                    mults.add((float) 0.5);
                } while (lastmult < 0.5);

                mults.add(lastmult);
            } else {
                mults.add(rate);
            }

            int count = 0;
            for (float tempo : mults) {
                if (count > 0) {
                    atempo += ",atempo=" + Float.toString(tempo);
                } else {
                    atempo += "atempo=" + Float.toString(tempo);
                }
                count++;
            }

            pb = new ProcessBuilder("./ffmpeg/bin/ffmpeg.exe", "-y", "-i", input, "-b:a", String.valueOf(bitrate)+"k", "-filter:a", atempo, "-vn", output);
        } else {
            Process p = null;
            ProcessBuilder b = new ProcessBuilder("./ffmpeg/bin/ffprobe.exe", "-i", input, "-show_streams");
            try {
                p = b.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader probe_output = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            int sample_rate = 44100;
            try {
                while ((line = probe_output.readLine()) != null) {
                    if(line.contains("sample_rate=")) {
                        sample_rate = Integer.valueOf(line.replace("sample_rate=",""));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            pb = new ProcessBuilder("./ffmpeg/bin/ffmpeg.exe", "-y", "-i", input, "-b:a", String.valueOf(bitrate)+"k", "-filter:a", "asetrate=" + Float.toString(sample_rate * rate), "-vn", output);
        }

        final Process p = pb.start();

        Runnable r = new RunMP3(p, index, window, rate);
        Thread t = new Thread(r);

        t.start();

    }

    public static class RunMP3 implements Runnable {

        private Process p;
        private int index;
        private Float rate;
        private MultipleRate window;

        public RunMP3(Process p, int index, MultipleRate window, Float rate) {
            this.p = p;
            this.index = index;
            this.window = window;
            this.rate = rate;
        }

        public void run() {
            Scanner sc = new Scanner(this.p.getErrorStream());

            // Find duration
            Pattern durPattern = Pattern.compile("(?<=Duration: )[^,]*");
            String dur = sc.findWithinHorizon(durPattern, 0);
            if (dur == null) {
                throw new RuntimeException("Could not parse duration.");
            }
            String[] hms = dur.split(":");
            double totalSecs = Integer.parseInt(hms[0]) * 3600
                    + Integer.parseInt(hms[1]) * 60
                    + Double.parseDouble(hms[2]);
            totalSecs = Math.floor((totalSecs / (double) rate));

            // Find time as long as possible.
            Pattern timePattern = Pattern.compile("(?<=time=)[\\d:.]*");
            String match;
            String[] matchSplit;
            while (null != (match = sc.findWithinHorizon(timePattern, 0))) {
                matchSplit = match.split(":");
                double progress = ((Integer.parseInt(matchSplit[0]) * 3600)
                        + (Integer.parseInt(matchSplit[1]) * 60)
                        + Math.floor(Double.parseDouble(matchSplit[2]))) / totalSecs;
                window.getProgresses().set(this.index, (float) progress);
                window.setProgress();
            }
        }
    }
}
