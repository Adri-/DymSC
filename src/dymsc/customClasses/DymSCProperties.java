/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dymsc.customClasses;

/**
 *
 * @author Adri
 */
public class DymSCProperties {

    // General
    public String osu_folder = "";

    // Rate
    public int default_bitrate = 128;
    public int default_music_selected = 1;
    public int default_nightcore_selected = 0;
    public String default_audio_name = "audio";

    // Packs
    public String default_author = "";
}
