/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dymsc.customClasses;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author Adri
 */
public class getPropertyValues {

    InputStream inputStream;

    public DymSCProperties getPropValues() throws IOException {
        DymSCProperties props = new DymSCProperties();
        try {
            System.out.println("Loading properties ...");
            Properties prop = new Properties();
            String propFileName = "config.properties";

            try {
                String propertyFileContents = readFile(propFileName, Charset.defaultCharset());
                inputStream = new FileInputStream(propFileName);
                prop.load(new StringReader(propertyFileContents.replace("\\", "\\\\")));
            } catch (Exception e) {
                System.out.println("config.properties does not exist, creating it ...");
                File configFile = new File("config.properties");
                configFile.createNewFile();

                ArrayList<String> text = new ArrayList<>();
                text.add("# DymSC Properties");
                text.add("# Do not put quotes ! Example:");
                text.add("# osu_folder=D:\\osu!");
                text.add("osu_folder=");
                text.add("");
                text.add("# Default Rate properties");
                text.add("default_bitrate=128");
                text.add("default_music_selected=1");
                text.add("default_nightcore_selected=0");
                text.add("default_audio_name=audio");
                text.add("");
                text.add("# Default Packs properties");
                text.add("default_author=");

                BufferedWriter writer = new BufferedWriter(new FileWriter(configFile));

                for (String line : text) {
                    System.out.println(line);
                    writer.write(line);
                    writer.newLine();
                }
                writer.close();
                System.out.println("Wrote config.properties");

                prop = new Properties();

                String propertyFileContents = readFile(propFileName, Charset.defaultCharset());
                inputStream = new FileInputStream(propFileName);

                if (inputStream != null) {
                    prop.load(new StringReader(propertyFileContents.replace("\\", "\\\\")));
                } else {
                    throw new FileNotFoundException("Property file '" + propFileName + "' not found and couldn't be created");
                }
            }

            props.osu_folder = prop.getProperty("osu_folder");
            System.out.println("osu_folder=" + props.osu_folder);

            try {
                props.default_bitrate = Integer.valueOf(prop.getProperty("default_bitrate"));
            } catch (Exception e) {
                props.default_bitrate = 128;
            }
            System.out.println("default_bitrate=" + Integer.toString(props.default_bitrate));
            try {
                props.default_music_selected = Integer.valueOf(prop.getProperty("default_music_selected"));
            } catch (Exception e) {
                props.default_music_selected = 1;
            }
            System.out.println("default_music_selected=" + Integer.toString(props.default_music_selected));
            try {
                props.default_nightcore_selected = Integer.valueOf(prop.getProperty("default_nightcore_selected"));
            } catch (Exception e) {
                props.default_nightcore_selected = 0;
            }
            System.out.println("default_nightcore_selected=" + Integer.toString(props.default_nightcore_selected));
            props.default_audio_name = prop.getProperty("default_audio_name");
            System.out.println("default_audio_name=" + props.default_audio_name);
            props.default_author = prop.getProperty("default_author");
            System.out.println("default_author=" + props.default_author);
            props.default_author = prop.getProperty("default_author");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return props;
    }

    static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
